# README #

Tarefas e exercicios de teste para seleção.

### TAREFAS DE 1 a 4 ###

* Tarefas de teste
* Version 1.0

### Tarefas ###

* Tarefa 1 - Concluida
* Tarefa 2 - Concluida
* Tarefa 3 - Concluida
* Tarefa 4 - Concluida

* Como testar a api
* Deployment instructions


  telas do sistema criado

![1.png](https://bitbucket.org/repo/Bryjpo/images/3583376610-1.png)

![2.png](https://bitbucket.org/repo/Bryjpo/images/952815960-2.png)

![3.PNG](https://bitbucket.org/repo/Bryjpo/images/1637833832-3.PNG)

![4.PNG](https://bitbucket.org/repo/Bryjpo/images/458435619-4.PNG)

![5.PNG](https://bitbucket.org/repo/Bryjpo/images/597308743-5.PNG)

![6.PNG](https://bitbucket.org/repo/Bryjpo/images/3414367836-6.PNG)


Modulos usados

AngularJS Bootstrap UI modal  
underscore.js
PHP Slim [Criação da API ]


Estrutura da aplicação

api [ serve os dados em Json ]

–          libs [ PHPSlim library ]

–          v1 [ API ]

**Sobre o client**

–          Escrita em angular e bootstrap para o visual, consumindo a nossa api, 
poderia ser escrita em varias outras linguagens e de n maneiras diferentes, interage completamente com a api.

**How to use**

Faça o download do projeto
Import o arquivo sql "tarefa.sql" no MySQL
Edite o arquivo "config.php" com as suas credenciais do banco de dados