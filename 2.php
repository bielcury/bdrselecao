<?php

session_start();

/*
Versão antiga
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

header("Location: http://www.google.com");

exit();

} elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {

header("Location: http://www.google.com");

exit();

}
*/


//Inicializando a session e o cookie
$_SESSION['loggedin'] = "true";

if( (isset($_SESSION['loggedin']) 
	&& $_SESSION['loggedin'] == true)
    || (isset($_COOKIE['loggedin']) 
    && $_COOKIE['loggedin'] == true)) 
{
    header("Location: http://www.google.com");
    exit();
} 
