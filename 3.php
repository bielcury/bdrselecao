<?php

/*
Versão antiga
class MyUserClass
{
public function getUserList()
{
$dbconn = new DatabaseConnection('localhost','user','password');
$results = $dbconn->query('select name from user');

sort($results);
return $results;
}
}
*/

class DatabaseConnection
{
	protected $database;

    public function __construct($server, $username, $password, $database){
       $this->database = new PDO("mysql:host={$server};dbname={$database}", $username, $password);
    }

    public function getConnection()
    {
        return $this->database;	
    }

    public function query($sql)
    {
   	    $query = $this->database->query($sql);
   	    return $query->fetchAll(PDO::FETCH_ASSOC);
    }


}

class MyUserClass
{

    public function getUserList()
    {
        $db = new DatabaseConnection('localhost','root','','bdr');
        //$dbconn   = $dbobject->getConnection();
        $results = $db->query("SELECT name FROM user ORDER BY name ASC");
        //Deixando essa função na query
        //sort($results);
        return $results;
    }
}


//testando
$getUsuario = new MyUserClass();
$usuario = $getUsuario->getUserList();

print_r($usuario);
