app.controller('productsCtrl', function ($scope, $modal, $filter, Data) {
    $scope.tarefa = {};
    Data.get('tarefas').then(function(data){
        $scope.tarefas = data.data;
    });


    $scope.changeTarefaStatus = function(tarefa){
        tarefa.prioridade = (tarefa.prioridade=="Alta" ? "Baixa" : "Alta");
        Data.put("tarefas/"+tarefa.id,{prioridade:tarefa.prioridade});
    };
    $scope.deleteTarefa = function(tarefa){
        if(confirm("Remover essa tarefa?")){
            Data.delete("tarefas/"+tarefa.id).then(function(result){
                $scope.tarefas = _.without($scope.tarefas, _.findWhere($scope.tarefas, {id:tarefa.id}));
            });
        }
    };
    $scope.open = function (p,size) {
        var modalInstance = $modal.open({
          templateUrl: 'partials/tarefaNew.html',
          controller: 'productEditCtrl',
          size: size,
          resolve: {
            item: function () {
              return p;
            }
          }
        });
        modalInstance.result.then(function(selectedObject) {
            if(selectedObject.save == "insert"){
                $scope.tarefas.push(selectedObject);
                $scope.tarefas = $filter('orderBy')($scope.tarefas, 'prioridade', 'Alta');
            }else if(selectedObject.save == "update"){
                p.titulo = selectedObject.titulo;
                p.descricao = selectedObject.descricao;

            }
        });
    };
    
 $scope.columns = [
                    {text:"Id",predicate:"id",sortable:true,dataType:"number"},
                    {text:"Titulo",predicate:"titulo",sortable:true},
                    {text:"Descricao",predicate:"descricao",sortable:true},
                    {text:"Prioridade",predicate:"prioridade",sortable:true},
                    {text:"Action",predicate:"",sortable:false}
                ];

});


app.controller('productEditCtrl', function ($scope, $modalInstance, item, Data) {

  $scope.tarefa = angular.copy(item);
        
        $scope.cancel = function () {
            $modalInstance.dismiss('Close');
        };
        $scope.title = (item.id > 0) ? 'Editar tarefa' : 'Adicionar tarefa';
        $scope.buttonText = (item.id > 0) ? 'Atualizar tarefa' : 'Cadastrar Nova tarefa';

        var original = item;
        $scope.isClean = function() {
            return angular.equals(original, $scope.tarefa);
        }
        $scope.saveTarefa = function (tarefa) {
            tarefa.uid = $scope.uid;
            if(tarefa.id > 0){
                Data.put('tarefas/'+tarefa.id, tarefa).then(function (result) {
                    if(result.status != 'error'){
                        var x = angular.copy(tarefa);
                        x.save = 'update';
                        $modalInstance.close(x);
                    }else{
                        console.log(result);
                    }
                });
            }else{
                tarefa.prioridade = 'Baixa';
                Data.post('tarefas', tarefa).then(function (result) {
                    if(result.prioridade != 'error'){
                        var x = angular.copy(tarefa);
                        x.save = 'insert';
                        x.id = result.data;
                        $modalInstance.close(x);
                    }else{
                        console.log(result);
                    }
                });
            }
        };
});
