<?php
require '.././libs/Slim/Slim.php';
require_once 'dbHelper.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app = \Slim\Slim::getInstance();
$db = new dbHelper();

// tarefas
$app->get('/tarefas', function() { 
    global $db;
    $rows = $db->select("tarefas","id,titulo,descricao,prioridade",array());
    echoResponse(200, $rows);
});

$app->post('/tarefas', function() use ($app) { 
    $data = json_decode($app->request->getBody());
    $mandatory = array('titulo');
    global $db;
    $rows = $db->insert("tarefas", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Tarefa adicionada com sucesso.";
    echoResponse(200, $rows);
});

$app->put('/tarefas/:id', function($id) use ($app) { 
    $data = json_decode($app->request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("tarefas", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Tarefa atualizada com sucesso.";
    echoResponse(200, $rows);
});

$app->delete('/tarefas/:id', function($id) { 
    global $db;
    $rows = $db->delete("tarefas", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Tarefa excluida com sucesso.";
    echoResponse(200, $rows);
});

function echoResponse($status_code, $response) {
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response,JSON_NUMERIC_CHECK);
}

$app->run();
